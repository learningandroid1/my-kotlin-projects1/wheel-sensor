fun main() {
    val wheel = Wheel()
    val pressure = 2.2

    try {
        wheel.pumpItUp(pressure)
        wheel.checkPressure()
    } catch (e: Exceptions.IncorrectPressure) {
        println("невозможно накачать $pressure атм, процедура не удалась")
    } catch (e: Exceptions.TooHighPressure) {
        println("Давление установлено $pressure атм, эксплуатация невозможна, давление превышает нормальное")
    } catch (e: Exceptions.TooLowPressure) {
        println("Давление установлено $pressure атм, эксплуатация невозможна, давление ниже нормального")
    }
}