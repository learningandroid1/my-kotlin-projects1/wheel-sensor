sealed class Exceptions(override val message: String): Throwable() {
    class TooHighPressure: Exceptions("Pressure is too high")
    class TooLowPressure: Exceptions("Pressure is too low")
    class IncorrectPressure: Exceptions("Pressure is not correct")
}