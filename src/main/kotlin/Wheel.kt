class Wheel {
    var currentPressure: Double = 0.0
    fun pumpItUp(pressure: Double){
        if (pressure in 0.0..10.00) currentPressure = pressure
        else throw Exceptions.IncorrectPressure()

    }

    fun checkPressure(){
        when (currentPressure) {
            in 0.0..1.6 -> throw Exceptions.TooLowPressure()
            in 2.5..10.0 -> throw Exceptions.TooHighPressure()
            in 1.6..2.5 -> println("Давление установлено $currentPressure атм, эксплуатация возможна")
            }
    }
}